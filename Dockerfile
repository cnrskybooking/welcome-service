FROM openjdk:12-oracle

# install maven
RUN yum -y install wget && \
    wget https://www-us.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz -P \tmp && \
    tar xf /tmp/apache-maven-3.6.3-bin.tar.gz -C /opt/ && \
    rm /tmp/apache-maven-3.6.3-bin.tar.gz && \
    ln -s /opt/apache-maven-3.6.3/ /opt/maven

ENV M2_HOME=/opt/maven
ENV MAVEN_HOME=/opt/maven
ENV PATH=/opt/maven/bin:${PATH}

WORKDIR /app
COPY . /app
RUN mvn clean && mvn package

EXPOSE 7777
#ENTRYPOINT ["java","-jar","/app.jar"]